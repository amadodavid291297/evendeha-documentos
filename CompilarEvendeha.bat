@echo off
cd C:\Desarrollo\Tecnologia\Flutter\eVendeha

echo "================================ SELECCIONAR TIPO DE COMPILACION ==============================="
echo 1. Compilacion de lanzamiento
echo 2. Compilacion por ABI
set /p opcion=Seleccione una opcion:

if %opcion%==1 (
  call  flutter build apk --release
) else if %opcion%==2 (
  call  flutter build apk --split-per-abi
) else (
    echo Opción no válida
    exit /b 1
)

:input_version
set /p version=Ingrese la version del APK (por ejemplo, 1.0.0):
if "%version%"=="" (
    echo Debe ingresar una version.
    goto input_version
)

echo "================================ Copiar archivos en carpeta local ==============================="
pause

rmdir /s /q C:\Users\amado\Documents\eVendeha-Documentos\APK
mkdir C:\Users\amado\Documents\eVendeha-Documentos\APK

if %opcion%==1 (
ECHO F | xcopy C:\Desarrollo\Tecnologia\Flutter\eVendeha\build\app\outputs\flutter-apk\app-release.apk C:\Users\amado\Documents\eVendeha-Documentos\APK\eVendeha-%version%.apk /y

) else if %opcion%==2 (
ECHO F | xcopy C:\Desarrollo\Tecnologia\Flutter\eVendeha\build\app\outputs\flutter-apk\app-arm64-v8a-release.apk C:\Users\amado\Documents\eVendeha-Documentos\APK\eVendeha-arm64-v8a-%version%.apk /y
ECHO F | xcopy C:\Desarrollo\Tecnologia\Flutter\eVendeha\build\app\outputs\flutter-apk\app-armeabi-v7a-release.apk C:\Users\amado\Documents\eVendeha-Documentos\APK\eVendeha-armeabi-v7a-%version%.apk /y
ECHO F | xcopy C:\Desarrollo\Tecnologia\Flutter\eVendeha\build\app\outputs\flutter-apk\app-x86_64-release.apk C:\Users\amado\Documents\eVendeha-Documentos\APK\eVendeha-x86_64-%version%.apk /y
)

echo "================================ Subir carpeta a Git =============================================="
set /p subir=¿Desea subir la carpeta C:\Users\amado\Documents\eVendeha-Documentos a Git? (s/n):
if /i "%subir%"=="s" (
    cd C:\Users\amado\Documents\eVendeha-Documentos
  call  git add .
  call  git commit -m "Agregado eVendeha-%version%.apk"
  call  git push origin main
) else (
    echo No se subira la carpeta a Git.
)
echo "================================ Presione ENTER para salir ===================================="
pause
